////////   IGW
resource "aws_internet_gateway" "main_internet_gateway" { # creem una porta de sortida a internet(GW)
  vpc_id = aws_vpc.main_vpc.id
  tags = {Name = "principal-igw"}
}

///////////  VPC

resource "aws_vpc" "main_vpc" {
  cidr_block           = "10.0.0.0/16"
  tags = {Name = "principal"}
  instance_tenancy  = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true
}

//NAT DE TOTA LA VIDA, SORTIR A INTERNET O ANAR A UNA ALTRE XARXA

/*
resource "aws_nat_gateway" "nat" {   // gràcies a aquest NAT les instancies de la subnet1 poden arribar a internet, però no al revés
  
  allocation_id = aws_eip.elastic.id   
  subnet_id = aws_subnet.public[0].id 
  depends_on    = [aws_internet_gateway.main_internet_gateway]
  tags = {
   Name        = "nat"  // nom com apareixerà a AWS
  }
}
*/
////////////// Public Elastic IP

resource "aws_eip" "elastic" {  //OJU AMB EL NOM, REPE !!
//public_ipv4_pool = "amazon"
 //associate_with_private_ip = "10.0.0.8"  // Assosiarem la IP elastica amb la IP privada 10.0.0.8
 //allocation_id
 count            = length(aws_instance.instance.*.id)  // 
 instance         = element(aws_instance.instance.*.id, count.index)
 vpc      = true
 depends_on                = [aws_internet_gateway.main_internet_gateway]
}

resource "aws_eip_association" "eip_association" {
  count         = length(aws_eip.elastic)
  instance_id   = element(aws_instance.instance.*.id, count.index)
  allocation_id = element(aws_eip.elastic.*.id, count.index)
}

resource "aws_subnet" "public" {
  vpc_id = aws_vpc.main_vpc.id
  count = "${length(var.subnet_cidr)}" 
  cidr_block = "${element(var.subnet_cidr,count.index)}"
  availability_zone       = "${element(var.Azones,count.index)}"
  map_public_ip_on_launch = true 
}
// ENRUTAMENT DE LA VPC EN GENERAL !!

variable subnet_cidr  { description = "LLista de subxarxes publiques" }

resource "aws_route_table" "main_rt" { # route table per a la subxarxa pública
  vpc_id = aws_vpc.main_vpc.id
  tags = { Name = "principal_public_rt"}
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id             = aws_internet_gateway.main_internet_gateway.id //000000 aws_nat_gateway.nat.id MAQ NO ACCESSIBLES
  }
}

resource "aws_route_table_association" "default" {
  count = "${length(var.subnet_cidr)}"
  subnet_id = "${element(aws_subnet.public.*.id,count.index)}"
  route_table_id = aws_route_table.main_rt.id  //comprobar
}

///////////////////////////////////////////////////////////////////////////

/*
resource "aws_network_interface" "loadbalancer"{
private_ips = ["10.0.0.8"] // ell ho te comentat
subnet_id =  aws_subnet.public[0].id               //triple
security_groups = [aws_security_group.permetre_http_s.id]
}

resource "aws_network_interface" "loadbalancer2"{
private_ips = ["10.0.1.8"] // ell ho te comentat
subnet_id =  aws_subnet.public[1].id               //triple
security_groups = [aws_security_group.permetre_http_s.id]
}

/*
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7

# SUBNET PUBLICA 1

/*
resource "aws_subnet" "public[1]" {
  vpc_id                  = aws_vpc.main_vpc.id # referenciem el valor de la "id" de la nostra subnet (main_vpc)
  cidr_block              = "10.0.0.0/24"  //cambiat 0 per 10
  map_public_ip_on_launch = true         # Quan inicï la "public subnet", a les instancies se lis assginarà una IP existent.
  availability_zone       = var.Azone # Escollim "a" per si necessitem utilitzar més "Availiability Zones" en un futur.
  tags = {Name = "principal-public"}   // DE FET AQUESTA ES LA NETWORK LOAD BALANCER QUE MENCIONEN
  depends_on = [aws_internet_gateway.main_internet_gateway]
}
*/

/*
resource "aws_route_table" "main_public_rt" { # route table per a la subxarxa pública
  //count="1"
  vpc_id = aws_vpc.main_vpc.id
  tags = {Name = "principal_public_rt"}
}

// Tot el que vulgui anar a 0.0.0.0/0 (fora) ha de passar pel gateway
resource "aws_route" "default_route_pu" {   // default_route, podem sortir on volguem

  route_table_id = aws_route_table.main_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main_internet_gateway.id
}

resource "aws_route_table_association" "main_public_assoc" { # vinculem la subxarxa publica a la taula d'enrutament
  count="1"
  subnet_id      = aws_subnet.public[0].id
  route_table_id = aws_route_table.main_public_rt.id
}
*/

# SUBNET PUBLICA 2
/*
resource "aws_subnet" "public[2]" {
  vpc_id                  = aws_vpc.main_vpc.id # referenciem el valor de la "id" de la nostra subnet (main_vpc)
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true         # Quan inicï la "public subnet", se li assginarà una IP pública existent.
  availability_zone       = var.Azone2 # Escollim "a" per si necessitem utilitzar més "Availiability Zones" en un futur.
  tags = {Name = "principal-public2"}   // DE FET AQUESTA ES LA NETWORK LOAD BALANCER QUE MENCIONEN
  depends_on = [aws_internet_gateway.main_internet_gateway]
}
*/

/*
resource "aws_route_table_association" "main_public_assoc2" { # vinculem la subxarxa publica a la taula d'enrutament
  
  subnet_id      = aws_subnet.public[1].id
  route_table_id = aws_route_table.main_public_rt.id
}

*/

/*
# SUBNET PRIVADA 1

resource "aws_subnet" "private1_subnet" {
  vpc_id                  = aws_vpc.main_vpc.id # Assignem el valor de la "id" de la nostra subnet (main_vpc)
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true         # Quan inicï la "public subnet", se li assginarà una IP pública existent.
  availability_zone       = var.Azone # Escollim "a" per si necessitem utilitzar més "Availiability Zones" en un futur.
  tags = {Name = "principal-private1"}
}

resource "aws_route_table" "main_private_rt" { # rt = route table

  vpc_id = aws_vpc.main_vpc.id
  tags =  { Name = "principal_private_rt" }
}

// Tot el que vulgui sortir fora de la xarxa privada ha de fer NAT
resource "aws_route" "default_route_pr" {   // default_route, podem sortir on volguem
  
  route_table_id         = aws_route_table.main_private_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_nat_gateway.nat.id
}

resource "aws_route_table_association" "main_private1_assoc" { 
# vinculem la subxarxa privada1 a la taula d'enrutament
 
  subnet_id      = aws_subnet.private1_subnet.id
  route_table_id = aws_route_table.main_private_rt.id
}

*/

/*
# SUBNET PRIVADA 2

resource "aws_subnet" "private2_subnet" {
  vpc_id                  = aws_vpc.main_vpc.id # Assignem el valor de la "id" de la nostra subnet (main_vpc)
  cidr_block              = "10.0.3.0/24"
  //map_public_ip_on_launch = false
  map_public_ip_on_launch = true         # Quan inicï la "public subnet", se li assginarà una IP pública existent.
  availability_zone       = var.Azone2 # Escollim "a" per si necessitem utilitzar més "Availiability Zones" en un futur.
  tags = {Name = "principal-private2"}
}

resource "aws_route_table_association" "main_private2_assoc" { 
# vinculem la subxarxa privada1 a la taula d'enrutament
  count = "1"
  subnet_id      = aws_subnet.private2_subnet.id
  route_table_id = aws_route_table.main_private_rt.id //cambiat
  
  }
*/



//////////////////////INTERFACE PER COMUNICA LA SUBETN1/ //////////////////////////////////////////////////////

/*
resource "aws_network_interface" "web-server"{

  subnet_id = aws_subnet.public_subnet.id
  private_ips = ["10.0.0.8"]  //IP de la subnet 1 !!
  security_groups = [aws_security_group.permetre_http_s.id]
 // attachment{
  //   instance = aws_instance.nginx1.id
  //}
}

resource "aws_network_interface" "web-server2"{

  subnet_id = aws_subnet.public[2].id
  private_ips = ["10.0.1.8"]  //IP de la subnet 1 !!
  security_groups = [aws_security_group.permetre_http_s.id]
 // attachment{
  //   instance = aws_instance.nginx1.id
  //}
}
*/